from django.contrib import admin

from .models import Order, DeliveryGroup, OrderLine, OrderHistoryEntry, OrderNote

admin.site.register(Order)
admin.site.register(DeliveryGroup)
admin.site.register(OrderLine)
admin.site.register(OrderHistoryEntry)
admin.site.register(OrderNote)
