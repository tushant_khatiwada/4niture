from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from .models import (Manufacturer, Category, ProductClass, Product, ProductVariant, StockLocation, Stock,
				ProductAttribute, AttributeChoiceValue, ProductImage, VariantImage)

admin.site.register(Manufacturer)
admin.site.register(ProductClass)
admin.site.register(Product)
admin.site.register(ProductVariant)
admin.site.register(StockLocation)
admin.site.register(Stock)
admin.site.register(ProductAttribute)
admin.site.register(AttributeChoiceValue)
admin.site.register(ProductImage)
admin.site.register(VariantImage)

class CategoryAdmin(MPTTModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}
    mptt_level_indent = 20

    class Meta:
        model = Category

admin.site.register(Category, CategoryAdmin)
