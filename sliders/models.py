from django.db import models
from markdownx.models import MarkdownxField

def upload_slider_image_path(instance, filename):
	return '4niture/sliders/{0}'.format(filename)

class Slider(models.Model):
	image = models.ImageField(null=True, blank=True, upload_to=upload_slider_image_path)
	caption = MarkdownxField(null=True)
	sub_caption = models.CharField(null=True, blank=True, max_length=100)
	button_url = models.URLField(blank=True, null=True, max_length=200)

	class Meta:
		verbose_name="Slider"
		verbose_name_plural="Sliders"

	def __str__(self):
		return self.image.url
