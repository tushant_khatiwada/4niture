from __future__ import unicode_literals

from django.conf.urls import url, include

from products.urls import urlpatterns as product_urls

from . import views


urlpatterns = [
	url(r'^$',
		views.home, name='home'),
	url(
		r'^products/',
		include((product_urls, 'product'), namespace='product')),
]
