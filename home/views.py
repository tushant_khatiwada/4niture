from django.shortcuts import render

from django.template.response import TemplateResponse

from products.utils import products_with_availability, products_for_homepage
from sliders.models import Slider


def home(request):
	products = products_for_homepage()[:8]
	products = products_with_availability(
		products, discounts=request.discounts, local_currency=request.currency)
	sliders = Slider.objects.all()
	context = {
		'products': products,
		'parent': None,
		'sliders': sliders
	}
	return TemplateResponse(
		request, 'home/home.html',
		context)
