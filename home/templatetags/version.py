from __future__ import unicode_literals

from django.template import Library
from personalized4niture import __version__

register = Library()


@register.simple_tag
def version():
    return __version__
